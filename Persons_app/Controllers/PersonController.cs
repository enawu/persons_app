﻿using Microsoft.AspNetCore.Mvc;
using Persons_app.Data;
using Persons_app.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace Persons_app.Controllers
{
    public class PersonController : Controller
    {
        private readonly ApplicationDBContex _db; //using dependency injection

        public PersonController(ApplicationDBContex db)
        {
            _db = db;
        }
        public IActionResult Index()
        {
            IEnumerable<Person> objList = _db.Person; // getting the list of all the persons

            return View(objList);
        }

        //GET - CREATE
        public IActionResult Create()
        {
            return View();
        }

        //POST - CREATE
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(Person obj)
        {
            try { 

                if (ModelState.IsValid)//validating on the server side
                {
                    _db.Person.Add(obj);
                    _db.SaveChanges();
                    return Json(obj);
                }
                else
                {
                    Person Empty_obj = new Person();
                    return Json(Empty_obj);//return empty object when 
                }

            }
            catch(Exception ex)
            {
                return Json(ex);
            }

}

   
        //export all the person in the database
         public IActionResult ExportToXML()
        {
            try { 
                
                    var data = _db.Person.ToList();//get all the persons

                    XmlDocument xml = new XmlDocument();  
                    XmlElement root = xml.CreateElement("Persons");  
                    xml.AppendChild(root);  
                    foreach (var person in data)  
                    {  
                        XmlElement child = xml.CreateElement("Person");  
                        child.SetAttribute("Username", person.username);  
                        child.SetAttribute("Phone", person.phone);  
                        child.SetAttribute("Country", person.country);  
                        child.SetAttribute("Comment", person.comments);  
                        root.AppendChild(child);  
                    }  
                    byte[] byteArray = ASCIIEncoding.ASCII.GetBytes(xml.OuterXml.ToString());  
 
                    Response.ContentType = "text/xml";  
                    Response.Headers.Add("Content-Disposition", "attachment; filename=PersonDetails.xml;");  
                    Response.Body.WriteAsync(byteArray);  
                    Response.Body.Flush();

                    return null;

            }
            catch(Exception ex)
            {
                return Json(ex);
            }

}
    }
}
