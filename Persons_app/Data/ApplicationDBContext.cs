﻿using Microsoft.EntityFrameworkCore;
using Persons_app.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Persons_app.Data
{
    public class ApplicationDBContex : DbContext
    {
        public ApplicationDBContex(DbContextOptions<ApplicationDBContex> options) : base(options)
        {

        }

        public DbSet<Person> Person{ get; set; }
    }
}
