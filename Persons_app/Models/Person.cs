﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Persons_app.Models
{
    public class Person
    {
        [Key]
        [DisplayName("User Name")]
        [StringLength(50)]
        [Required]
         public string username { get; set; }

        [DisplayName("Phone Number")]
        [StringLength(20)]
        [Required]
        public string phone { get; set; }

        [DisplayName("Address")]
        [StringLength(250)]
        [Required]
        public string address { get; set; }

        [DisplayName("Country")]
        [StringLength(50)]
        [Required]
        public string country { get; set; }

        [DisplayName("Comment")]
        public string comments { get; set; }
    }
}
